---
title: C++ quick exploration of threads
author: Apolexian
date: 2020-05-15 12:00:00 +0100
categories: [cpp]
tags: [cpp]
---

Synchronization primitives exist in C++ in the form of:

* Atomic
* Mutex
* Semaphore
* Memory fence
* Transactional memory (might come in the future)

Considering the following:

```cpp
#include <iostream>
#include <thread>

using namespace std;

int main() {
    int value = 42;

    auto t1 = thread{ [&value]{ ++value; } };
    auto t2 = thread{ [&value]{ value *= 2; } };
  
    t1.join();
    t2.join();

    cout << value << endl;
}
```

What are the possible outputs?

* 43
* 85
* 84
* 86
* it is actually machine depended and not clearly defined

Lets now consider the same snippet using atomic:

```cpp
#include <atomic>
#include <iostream>
#include <thread>

using namespace std;

int main() {
    atomic_int value{42};

    auto t1 = thread{ [&value]{ ++value; } };
    auto t2 = thread{ [&value]{ value = value * 2; } };
  
    t1.join();
    t2.join();

    cout << value << endl;
}
```

What is the output now? A possibility of:

* 84
* 85
* 86

What about using mutex?

```cpp
int main() {
    int value{42};
    mutex m;
    auto t1 = thread{ [&] {
        unique_lock block{m};
        ++value;
    } };
    auto t2 = thread{ [&] { 
        unique_lock block{m};
        value *= 2;
    } };
    t1.join(); t2.join();
    cout << value << endl;
}
```

What is the output now? A possibility of:

* 85
* 86
