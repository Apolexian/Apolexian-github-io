---
title: Ramblings about internship
author: Apolexian
date: 2020-11-22 17:00:00 +0100
categories: [life]
tags: [internship]
---

## Some rambling and thoughts about my summer 2020 Internship

This will mainly serve as a time capsule of sorts for myself and a collection of random thoughts on some of the more interesting technical problems I encountered during my summer 2020 internship at Autodesk.

I mainly worked on 3D printer connectivity in the realm of Fusion 360.

### Printers and slicing

During the course of the summer I got to tinker with two 3D printers, the [Bibo](https://all3dp.com/1/bibo-3d-printer-review-specs/), until I sent it back due to some technical complications about after a week of debugging it and the more popular and standard [Creality Ender 3](https://www.creality3dofficial.com/products/official-creality-ender-3-3d-printer), which I had for the vast majority of the duration.

Turns out that debugging hardware is a whole lot harder than running through something with a debugger. Especially when it came to the Bibo, the instructions were few and sparse and having no prior experience with 3D Printers, fixing one of the extruder motors not working was pretty much impossible for me. With the creality, things were quite a bit easier, mainly due to the vast information available and the tech consultants at Autodesk having prior knowledge of the machine.

In terms of slicing, the Fusion interface turned out to be a whole lot more confusing than cura, so cura was pretty much the go to for the majority of prints I did except for the test runs for the software I was working on during the summer.

3D Printing was quite fun for the first 10-15 models that I tried, after that the novelty quickly wore off. However, the usefulness of the technology in an actual practical sense, be it for workshops or manufacturing seems undeniable.

### The Fusion360 API

The [Fusion360 API](https://autodeskfusion360.github.io/) is definitely very powerful in allowing to do menial tasks in an automated manner and to create scripts. However, when trying to create a more sophisticated piece of software, such as a connectivity add-in, a few frustrations definitely set in.

The situation with the python interpreter is probably the single most frustrating part of the API. The inability to easily use and ship third party libraries (anything to do with pip) renders a huge advantage of python (number of libraries) inexistent. I don't see how this is not top priority for the API developers (maybe it is?..).

The documentation is mostly fine, although again, covering the most basic use cases with no examples for anything slightly harder. Understanding how to do anything sophisticated using palettes pretty much has to come from trial and error.

Multiprocessing not being possible due to the Fusion python interpreter not being able to gain necessary permissions on windows to spawn processes was also quite a bummer. Threads run into the global interpreter lock and there is seemingly no way to go around this without multiprocessing (possibly should just use the c++ API instead of python).
