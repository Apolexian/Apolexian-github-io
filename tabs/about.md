---
title: About

# The About page
# v2.0
# https://github.com/cotes2020/jekyll-theme-chirpy
# © 2017-2019 Cotes Chung
# MIT License
---

## A bit about me

My CV is [here](../assets/Ivan_Nikitin_Resume.pdf).

I am currently studying for a BSC (Hons) in Computer Science at the University of Glasgow. My general interests reflect in the courses I took:

* Third year elective courses: Text as Data, Theory of Computation, Robotics Foundation, Database Systems.

## Experience

### General Teaching Assistant - Univeristy of Glasgow September 2020 to present

Helped students during their univeristy labs with their coursework and any questions about course topics. Courses I demonstrated on:

* CS1F - Computer Science fundamentals. This included SQL, database theory and an HCI component.

### Software Engineering Intern - Autodesk June 2020 to September 2020

Worked in an agile team with my project focusing on extending FFF machine connectivity in [Fusion 360](https://www.autodesk.co.uk/products/fusion-360/overview). Used lots of REST APIs, parallel processing and async network calls. Mainly Python, some C++ and a small bit in Javascript and HTML.

### Spring Intern - Goldman Sachs May 2020

Attended a variety of sessions centered around software engineering and business as part of the Spring Internship Programme. Worked on a research project and presentation focused on the deployment phase of the software development life cycle, CI/CD, IaC and containerization through Docker.

## Some societies I am involved in

I am currently the secretary of the board of the Glasgow University Security Society. We are a society focused on teaching students
about the importance of cyber security and giving them a hands on experience. Check us out on our social media pages:

* [Our Twitter page](https://twitter.com/GUSecurity)
* [Our Facebook page](https://www.facebook.com/GUSecSoc/)
* [Our Linkedin page](https://www.linkedin.com/company/glasgow-university-security-society/)

I am also a current advisory for the board of the Glasgow University Tech Society and previously
was serving as vice president for it. GUTS focuses on organising large scale tech events
for students, like hackathons and other. If you want to know more about GUTS or get in touch:

* [Our website](https://gutechsoc.com/)
* [Our Facebook](https://www.facebook.com/gutechsoc/)
